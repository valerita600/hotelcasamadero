const express = require('express');
const router = express.Router();
const pool = require('../database');
var dateTime = require('node-datetime');
const { isLoggedIn } = require('../lib/auth');

router.post('/agregar_idioma', async (req, res) => {
    var dt = dateTime.create();
    var formatted = dt.format('Y-m-d H:M:S');
    const {nombre_idioma, activo_idioma} = req.body;
    const created_at_idioma = formatted;
    const updated_at_idioma = formatted;
    let idioma_nuevo = {nombre_idioma, activo_idioma, created_at_idioma, updated_at_idioma};
    await pool.query("INSERT INTO idioma SET ?", idioma_nuevo);

    //obtecion de usuario
    const Usuario_id_usuario = req.session.passport.user;
    const datos = await pool.query("SELECT * from empleado WHERE Usuario_id_usuario = ?", Usuario_id_usuario);
    //insercion de bitacora
    const fecha = formatted;
    const Tablas_id_tabla = 2;//cambiar
    const accion = "creacion";
    const descripcion = "el usuario "+ datos[0].nombre_empleado 
                        +" creo idioma "+ nombre_idioma 
                        +" con fecha " +created_at_idioma;
    let accion_bitacora = { descripcion, Tablas_id_tabla, accion, fecha, Usuario_id_usuario};
    await pool.query("INSERT INTO bitacora SET ?", accion_bitacora);

    res.send("idioma agregado");
});

module.exports = router;