const express = require('express');
const router = express.Router();
const pool = require('../database');
var dateTime = require('node-datetime');
const { isLoggedIn } = require('../lib/auth');

router.post('/agregar_cama', isLoggedIn, async (req, res) => {
    //variable para Id de usuario logueado
    //req.session.passport.user
    //insercion de cama
    var dt = dateTime.create();
    var formatted = dt.format('Y-m-d H:M:S');
    const {nombre_tipo_cama, descripcion_tipo_cama, cantidad_tipo_cama, capacidad_tipo_cama} = req.body;
    const created_at_tipo_cama = formatted;
    const updated_at_tipo_cama = formatted;
    let cama_nueva = {nombre_tipo_cama, descripcion_tipo_cama, cantidad_tipo_cama, capacidad_tipo_cama, created_at_tipo_cama, updated_at_tipo_cama};
    await pool.query("INSERT INTO tipos_cama SET ?", cama_nueva);
    //obtecion de usuario
    const Usuario_id_usuario = req.session.passport.user;
    const datos = await pool.query("SELECT * from empleado WHERE Usuario_id_usuario = ?", Usuario_id_usuario);
    //insercion de bitacora
    const fecha = formatted;
    const Tablas_id_tabla = 1;//cambiar
    const accion = "creacion";
    const descripcion = "el usuario "+ datos[0].nombre_empleado 
                        +" creo una cama tipo "+ nombre_tipo_cama 
                        +" con fecha " +created_at_tipo_cama;
    let accion_bitacora = { descripcion, Tablas_id_tabla, accion, fecha, Usuario_id_usuario};
    await pool.query("INSERT INTO bitacora SET ?", accion_bitacora);
    
    res.send("cama agregada");
});

module.exports = router;