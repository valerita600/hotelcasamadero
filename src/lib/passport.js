const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const pool = require('../database');
const helpers = require('./helpers');

passport.use('local.signin', new LocalStrategy({
    usernameField: 'correo_usuario',
    passwordField: 'contra_usuario',
    passReqToCallback: true
}, async(req, correo_usuario, contra_usuario, done) => {
    const rows = await pool.query('SELECT * FROM usuario WHERE correo_usuario = ?', [correo_usuario]);
    if (rows.length > 0) {
        const user = rows[0];
        const validPassword = await helpers.matchPassword(contra_usuario, user.contra_usuario)
        console.log(contra_usuario);
        console.log(user.contra_usuario);
        console.log(validPassword);
        if (validPassword) {
            done(null, user, req.flash('success', 'Welcome ' + user.username));
        } else {
            done(null, false, req.flash('message', 'Incorrect Password'));
        }
    } else {
        return done(null, false, req.flash('message', 'The Username does not exists.'));
    }
}));

passport.use('local.signup', new LocalStrategy({
    usernameField: 'correo_usuario',
    passwordField: 'contra_usuario',
    passReqToCallback: true
}, async(req, correo_usuario, contra_usuario, done) => {

    let Rol_id_rol = 2;
    let newUser = {
        correo_usuario,
        contra_usuario,
        Rol_id_rol
    };
    newUser.contra_usuario = await helpers.encryptPassword(contra_usuario);
    // Saving in the Database
    const result = await pool.query('INSERT INTO usuario SET ? ', newUser);
    newUser.id_usuario = result.insertId;
    return done(null, newUser);
}));

passport.serializeUser((user, done) => {
    done(null, user.id_usuario);
});

passport.deserializeUser(async(id, done) => {
    const rows = await pool.query('SELECT * FROM usuario WHERE id_usuario = ?', [id]);
    done(null, rows[0]);
});